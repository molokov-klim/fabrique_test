# Generated by Django 4.0.6 on 2022-07-31 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0006_alter_customer_tag'),
    ]

    operations = [
        migrations.CreateModel(
            name='BackgroundTask',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('task_name', models.CharField(max_length=190)),
                ('task_params', models.TextField()),
                ('task_hash', models.CharField(max_length=40)),
                ('verbose_name', models.CharField(blank=True, max_length=255, null=True)),
                ('priority', models.IntegerField()),
                ('run_at', models.DateTimeField()),
                ('repeat', models.BigIntegerField()),
                ('repeat_until', models.DateTimeField(blank=True, null=True)),
                ('queue', models.CharField(blank=True, max_length=190, null=True)),
                ('attempts', models.IntegerField()),
                ('failed_at', models.DateTimeField(blank=True, null=True)),
                ('last_error', models.TextField()),
                ('locked_by', models.CharField(blank=True, max_length=64, null=True)),
                ('locked_at', models.DateTimeField(blank=True, null=True)),
                ('creator_object_id', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'background_task',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DjangoContentType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_label', models.CharField(max_length=100)),
                ('model', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'django_content_type',
                'managed': False,
            },
        ),
    ]
