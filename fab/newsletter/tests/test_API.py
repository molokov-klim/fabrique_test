from rest_framework import status
from rest_framework.test import APIRequestFactory, URLPatternsTestCase
from django.urls import include, path
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from newsletter.tests.service import *
from newsletter.models import Customer, Mailing, MailingFlag, Message
from newsletter.core.operational import *

tjson = TestJSON()
gdata = GenerateData()


class BaseModelTestCase(APITestCase):
    def setUp(self):
        qty = 10  # размер тестовых данных (10-1000)
        data = gdata.get_data(qty)

        # Fill Customer table
        users = gdata.get_customers(data)
        for user in users:
            Customer.objects.create(phone=user['phone'], operator_code=user['operator_code'], tag=user['tag'],
                                    UTC=user['UTC'])

        # Fill Mailing table (and MailingFlag)
        mailing = gdata.get_mailing(data)
        for mail in mailing:
            Mailing.objects.create(send_datetime=mail['send_datetime'], send_text=mail['send_text'],
                                   filter_customer=mail['filter_customer'],
                                   stop_send_datetime=mail['stop_send_datetime'])
        for mail in Mailing.objects.all():
            MailingFlag.objects.create(status=False, mailing_id=mail.pk)

        # Fill Message table
        sending = Sending()
        sending.handle_mailing()


class CustomerGETTestCase(BaseModelTestCase):
    def test_get_customer(self):
        client = APIClient()
        url = reverse('customer-list')
        response = client.get(url, format='json')
        json_obj = json.loads(response.content)[0]
        schema = tjson.get_schema('./newsletter/tests/json_example_customer.json')
        result = tjson.validate_json(json_obj, schema)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(True, result)


class CustomerPOSTTestCase(BaseModelTestCase):
    def test_post_customer(self):
        client = APIClient()
        url = reverse('customer-list')
        data = {'phone': "79654449192", 'operator_code': 965, 'tag': "root", 'UTC': "+3"}
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Customer.objects.get(phone='79654449192').phone, '79654449192')


class CustomerGETIDTestCase(BaseModelTestCase):
    def test_get_id_customer(self):
        client = APIClient()
        data_object = Customer.objects.all()[:1]
        customer_id = str(data_object[0].id)
        url = reverse('customer-detail', kwargs={"pk": customer_id})
        response = client.get(url, format='json', follow=True)
        json_obj = json.loads(response.content)
        schema = tjson.get_schema('./newsletter/tests/json_example_customer.json')
        result = tjson.validate_json(json_obj, schema)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(True, result)


class CustomerPUTTestCase(BaseModelTestCase):
    def test_put_customer(self):
        client = APIClient()
        data_object = Customer.objects.all()[:1]
        customer_id = str(data_object[0].id)
        url = reverse('customer-detail', kwargs={"pk": customer_id})
        data = {'phone': '79654449192', 'operator_code': '965', 'tag': 'root', 'UTC': '+3'}
        response = client.put(path=url, data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.get(phone='79654449192').phone, '79654449192')


class CustomerPATCHTestCase(BaseModelTestCase):
    def test_patch_customer(self):
        client = APIClient()
        data_object = Customer.objects.all()[:1]
        customer_id = str(data_object[0].id)
        url = reverse('customer-detail', kwargs={"pk": customer_id})
        data = {'phone': '79654449192', 'operator_code': '965'}
        response = client.patch(path=url, data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Customer.objects.get(phone='79654449192').phone, '79654449192')


class CustomerDELETETestCase(BaseModelTestCase):
    def test_delete_customer(self):
        client = APIClient()
        data_object = Customer.objects.all()[:1]
        customer_id = str(data_object[0].id)
        url = reverse('customer-detail', kwargs={"pk": customer_id})
        response = client.delete(path=url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(Customer.objects.filter(id=customer_id)), 0)


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class MailingGETTestCase(BaseModelTestCase):
    def test_get_mailing(self):
        client = APIClient()
        url = reverse('mailing-list')
        response = client.get(url, format='json')
        json_obj = json.loads(response.content)[0]
        schema = tjson.get_schema('./newsletter/tests/json_example_mailing.json')
        result = tjson.validate_json(json_obj, schema)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(True, result)


class MailingPOSTTestCase(BaseModelTestCase):
    def test_post_mailing(self):
        client = APIClient()
        url = reverse('mailing-list')
        data = {
            'send_datetime': timezone.now() + timezone.timedelta(days=-1),
            'send_text': "test text message",
            'filter_customer': "test_filter_customer",
            'stop_send_datetime': timezone.now() + timezone.timedelta(days=1)
        }
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Mailing.objects.get(filter_customer='test_filter_customer').filter_customer,
                         'test_filter_customer')


class MailingGETIDTestCase(BaseModelTestCase):
    def test_get_id_mailing(self):
        client = APIClient()
        data_object = Mailing.objects.all()[:1]
        mailing_id = str(data_object[0].id)
        url = reverse('mailing-detail', kwargs={"pk": mailing_id})
        response = client.get(url, format='json', follow=True)
        json_obj = json.loads(response.content)
        schema = tjson.get_schema('./newsletter/tests/json_example_mailing.json')
        result = tjson.validate_json(json_obj, schema)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(True, result)


class MailingPUTTestCase(BaseModelTestCase):
    def test_put_mailing(self):
        client = APIClient()
        data_object = Mailing.objects.all()[:1]
        mailing_id = str(data_object[0].id)
        url = reverse('mailing-detail', kwargs={"pk": mailing_id})
        data = {
            'send_datetime': timezone.now() + timezone.timedelta(days=-1),
            'send_text': "test text message",
            'filter_customer': "test_filter_customer",
            'stop_send_datetime': timezone.now() + timezone.timedelta(days=1)
        }
        response = client.put(path=url, data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Mailing.objects.get(filter_customer='test_filter_customer').filter_customer,
                         'test_filter_customer')


class MailingPATCHTestCase(BaseModelTestCase):
    def test_patch_mailing(self):
        client = APIClient()
        data_object = Mailing.objects.all()[:1]
        mailing_id = str(data_object[0].id)
        url = reverse('mailing-detail', kwargs={"pk": mailing_id})
        data = {
            'send_text': "test text message",
            'filter_customer': "test_filter_customer",
        }
        response = client.patch(path=url, data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Mailing.objects.get(filter_customer='test_filter_customer').filter_customer,
                         'test_filter_customer')


class MailingDELETETestCase(BaseModelTestCase):
    def test_delete_mailing(self):
        client = APIClient()
        data_object = Mailing.objects.all()[:1]
        mailing_id = str(data_object[0].id)
        url = reverse('mailing-detail', kwargs={"pk": mailing_id})
        response = client.delete(path=url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(len(Mailing.objects.filter(id=mailing_id)), 0)

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class StatMailingAllTestCase(BaseModelTestCase):
    def test_stat_mailing_all(self):
        client = APIClient()
        url = reverse('stat_mailing_all')
        response = client.get(url, format='json')
        json_obj = json.loads(response.content)[0]
        schema = tjson.get_schema('./newsletter/tests/json_example_stat_mailing_all.json')
        result = tjson.validate_json(json_obj, schema)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(True, result)


class StatMailingSelectedTestCase(BaseModelTestCase):
    def test_stat_mailing_selected(self):
        client = APIClient()
        data_object = Mailing.objects.all()[:1]
        mailing_id = str(data_object[0].id)
        url = reverse('stat_mailing_selected', kwargs={"pk": mailing_id})
        response = client.get(url, format='json', follow=True)
        json_obj = json.loads(response.content)[0]
        schema = tjson.get_schema('./newsletter/tests/json_example_message.json')
        result = tjson.validate_json(json_obj, schema)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(True, result)


