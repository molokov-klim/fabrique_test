# from newsletter.core.background import loop_sending, loop_handle_mailing, truncate_background_task
# import subprocess

from django.test import TestCase
from newsletter.core.operational import *
from newsletter.tests.service import *
from newsletter.models import Customer, Mailing, MailingFlag, Message
import datetime
import logging
logging.basicConfig(filename='test.log', filemode='w', level=logging.DEBUG, format='%(name)s - %(levelname)s - %(message)s')

aux = Auxiliary()
send = Sending()
tjson = TestJSON()
gdata = GenerateData()


class BaseModelTestCase(TestCase):
    def setUp(self):
        qty = 10  # размер тестовых данных (10-1000)
        data = gdata.get_data(qty)

        # Fill Customer table
        users = gdata.get_customers(data)
        for user in users:
            Customer.objects.create(phone=user['phone'], operator_code=user['operator_code'], tag=user['tag'],
                                    UTC=user['UTC'])

        # Fill Mailing table (and MailingFlag)
        mailing = gdata.get_mailing(data)
        for mail in mailing:
            Mailing.objects.create(send_datetime=mail['send_datetime'], send_text=mail['send_text'],
                                   filter_customer=mail['filter_customer'],
                                   stop_send_datetime=mail['stop_send_datetime'])
        for mail in Mailing.objects.all():
            MailingFlag.objects.create(status=False, mailing_id=mail.pk)

        # Fill Message table
        sending = Sending()
        sending.handle_mailing()

class AuxiliaryValidatePhoneTestCase(TestCase):
    def test_validate_phone_positive(self):
        result = aux.validate_phone("79654449192")
        self.assertEqual(True, result)

    def test_validate_phone_negative(self):
        result = aux.validate_phone("9654449192")
        self.assertEqual(False, result)

class AuxiliaryExtractOperatorCode(TestCase):
    def test_extract_operator_code(self):
            result = aux.extract_operator_code("79654449192")
            self.assertEqual("965", result)

class AuxiliaryConvertToDatetime(TestCase):
    def test_convert_to_datetime(self):
            result = type(aux.convert_to_datetime("2022-05-05 11:42:00"))
            self.assertEqual(datetime.datetime, result)

class AuxiliaryStatMailingAll(BaseModelTestCase):
    def test_stat_mailing_all(self):
            list_of_dicts = aux.stat_mailing_all()
            schema = tjson.get_schema('./newsletter/tests/json_example_stat_mailing_all.json')
            result = None
            for i in list_of_dicts:
                result = tjson.validate_json(i, schema)
            self.assertEqual(True, result)

class AuxiliaryStatMailingSelected(BaseModelTestCase):
    def test_stat_mailing_selected(self):
            data_object = Mailing.objects.all()[:1]
            list_of_dicts = aux.stat_mailing_selected(data_object[0].id)
            schema = tjson.get_schema('./newsletter/tests/json_example_message.json')
            result = None
            for i in list_of_dicts:
                result = tjson.validate_json(i, schema)
            self.assertEqual(True, result)

class SendingUpdateMailingStatus(BaseModelTestCase):
    def test_update_mailing_status(self):
            Message.objects.filter(status="False").update(status="True")
            before = MailingFlag.objects.filter(status=False).count()
            send.update_mailing_status()
            after = MailingFlag.objects.filter(status=True).count()
            self.assertEqual(before, after)

class SendingCreateMailingFlagRows(BaseModelTestCase):
    def test_create_mailing_flag_rows(self):
            before = MailingFlag.objects.all().count()
            MailingFlag.objects.all().delete()
            send.create_mailing_flag_rows()
            after = MailingFlag.objects.all().count()
            self.assertEqual(before, after)

class SendingExtractRabgeFromMailing(BaseModelTestCase):
    def test_extract_range_from_mailing(self):
            logging.debug(send.extract_range_from_mailing())
            list_of_dicts = send.extract_range_from_mailing()
            schema = tjson.get_schema('./newsletter/tests/json_example_mailing.json')
            result = None
            for i in list_of_dicts:
                result = tjson.validate_json(i, schema)
            self.assertEqual(True, result)

class SendingExtractRangeFromCustomer(BaseModelTestCase):
    def test_extract_range_from_customer(self):
            data_object = Mailing.objects.all()[:1]
            filter_customer = data_object[0].filter_customer
            list_of_dicts = send.extract_range_from_customer(filter_customer)
            schema = tjson.get_schema('./newsletter/tests/json_example_customer.json')
            result = None
            for i in list_of_dicts:
                result = tjson.validate_json(i, schema)
            self.assertEqual(True, result)

class SendingDefineFilter(BaseModelTestCase):
    def test_define_filter(self):
            tag = send.define_filter("qwertyuiop")
            operator_code = send.define_filter("965")
            self.assertEqual("tag", tag)
            self.assertEqual("operator_code", operator_code)

class SendingCreateMessage(BaseModelTestCase):
    def test_create_message(self):
            for i in Mailing.objects.all():
                row_mailing = model_to_dict(i)
                for y in Customer.objects.all():
                    row_customer = model_to_dict(y)
                    send.create_message(row_mailing, row_customer)
            before = Message.objects.all().count()
            Message.objects.all().delete()
            for i in Mailing.objects.all():
                row_mailing = model_to_dict(i)
                for y in Customer.objects.all():
                    row_customer = model_to_dict(y)
                    send.create_message(row_mailing, row_customer)
            after = Message.objects.all().count()
            self.assertEqual(before, after)

class SendingHandleMailing(BaseModelTestCase):
    def test_handle_mailing(self):
            send.handle_mailing()
            before = Message.objects.all().count()
            Message.objects.all().delete()
            send.handle_mailing()
            after = Message.objects.all().count()
            self.assertEqual(before, after)

class SendingSendToCloud(BaseModelTestCase):
    def test_send_to_cloud(self):
            resp = send.send_to_cloud(18, "text", "79654449192")
            self.assertEqual(True, resp)





#------------------------------------------------------------------------------------------------------------------
#   doesn't work, fix later (problem - subprocess work with real db, dont know how to test infinite loop)
#
# class BackgroundTestCases(TestCase):
#     def setUp(self):
#         qty = 10  # размер тестовых данных
#         data = gdata.get_data(qty)
#
#         # Fill Customer table
#         users = gdata.get_customers(data)
#         for user in users:
#             Customer.objects.create(phone=user['phone'], operator_code=user['operator_code'], tag=user['tag'],
#                                     UTC=user['UTC'])
#
#         # Fill Mailing table (and MailingFlag)
#         mailing = gdata.get_mailing(data)
#         for mail in mailing:
#             Mailing.objects.create(send_datetime=mail['send_datetime'], send_text=mail['send_text'],
#                                    filter_customer=mail['filter_customer'],
#                                    stop_send_datetime=mail['stop_send_datetime'])
#         for mail in Mailing.objects.all():
#             MailingFlag.objects.create(status=False, mailing_id=mail.pk)
#
#         # Fill Message table
#         sending = Sending()
#         sending.handle_mailing()
#
#
#     def test_loop_sending(self):
#         mailing_object = Mailing.objects.all()[:1]
#         mailing_id = mailing_object[0].pk
#         customer_object = Customer.objects.all()[:1]
#         customer_id = customer_object[0].pk
#         Message.objects.all().delete()
#         Message.objects.create(time_create=timezone.now(), status=False, customer_id=customer_id, mailing_id=mailing_id)
#         loop_sending(repeat=25)
#         process = subprocess.Popen(['python', 'manage.py', 'process_tasks'])
#         message_object = Message.objects.all()[:1]
#         message_status = message_object[0].status
#         try:
#             process.wait(timeout=30)
#         except subprocess.TimeoutExpired:
#             process.kill()
#         self.assertEqual(True, message_status)

