from django.test import TestCase
from newsletter.models import Customer, Mailing, MailingFlag, Message
from django.utils import timezone


class BaseModelTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(BaseModelTestCase, cls).setUpClass()
        cls.customer = Customer(phone="79654449192", operator_code="965", tag="root", UTC="+3")
        cls.customer.save()
        cls.mailing = Mailing(send_datetime=timezone.now() + timezone.timedelta(days=-1), send_text="test text message",
                              filter_customer="root", stop_send_datetime=timezone.now() + timezone.timedelta(days=1))
        cls.mailing.save()
        cls.mailingflag = MailingFlag(status=False, mailing=cls.mailing)
        cls.mailingflag.save()
        cls.message = Message(time_create=timezone.now(), status=False, mailing=cls.mailing, customer=cls.customer)
        cls.message.save()


class CustomerModelTestCase(BaseModelTestCase):
    def test_created_properly(self):
        self.assertEqual(self.customer.phone, '79654449192')
        self.assertEqual(True, self.message in self.customer.message_set.all())

    def test_absolute_url(self):
        self.assertEqual(self.customer.get_absolute_url(), f"/customer/{self.customer.pk}")

class MailingModelTestCase(BaseModelTestCase):
    def test_created_properly(self):
        self.assertEqual(self.mailing.filter_customer, 'root')
        self.assertEqual(True, self.message in self.mailing.message_set.all())

    def test_absolute_url(self):
        self.assertEqual(self.mailing.get_absolute_url(), f"/mailing/{self.mailing.pk}")

class MailingFlagModelTestCase(BaseModelTestCase):
    def test_created_properly(self):
        self.assertEqual(self.mailingflag.status, False)
        self.assertEqual(True, self.mailingflag in self.mailing.mailingflag_set.all())

class MessageModelTestCase(BaseModelTestCase):
    def test_created_properly(self):
        self.assertEqual(self.message.status, False)
        self.assertEqual(1, len(Message.objects.filter(status=False)))

    def test_absolute_url(self):
        self.assertEqual(self.message.get_absolute_url(), f"/message/{self.message.pk}")


