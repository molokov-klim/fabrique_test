import json
import jsonschema
import requests
from jsonschema import validate
from genson import SchemaBuilder
from newsletter.core.operational import Auxiliary
from datetime import datetime, timedelta
from django.utils import timezone
import pytz

aux = Auxiliary()


class DateTimeEncoder(json.JSONEncoder):
    def default(self, z):
        if isinstance(z, datetime):
            return (str(z))
        else:
            return super().default(z)


class TestJSON:
    def __init__(self):
        pass

    def get_schema(self, path):
        """This function loads the given schema available"""
        builder = SchemaBuilder()
        with open(path, 'r') as file:
            datastore = json.load(file)
            builder.add_object(datastore)

        schema = builder.to_schema()
        return schema

    def validate_json(self, jsonData, jsonSchema):
        try:
            jsonData = json.dumps(jsonData, cls=DateTimeEncoder)
            jsonData = json.loads(jsonData)
            validate(instance=jsonData, schema=jsonSchema)
        except jsonschema.exceptions.ValidationError as err:
            print("[SYS] Error: ", err)
            return False
        return True


class GenerateData:
    def __init__(self):
        pass

    def get_data(self, qty):
        basic_url = 'http://api.randomuser.me/?results='
        url = basic_url + str(qty)
        res = requests.request("GET", url)
        res = res.json()
        return res

    def get_customers(self, res):
        users = []
        for user in res["results"]:
            phone = "7" + GenerateData.remove_characters_except_digits(user['cell'])
            operator_code = aux.extract_operator_code(phone)
            tag = user['gender']
            UTC = user['location']['timezone']['offset']
            phone = phone[0:11]
            if len(phone) == 11:
                users.append({
                    "phone": phone,
                    "operator_code": operator_code,
                    "tag": tag,
                    "UTC": UTC,
                })
        return users

    @staticmethod
    def remove_characters_except_digits(data):
        data = ''.join(i for i in data if i.isdigit())
        return data

    def get_mailing(self, res):
        mailing = []
        yesterday = timezone.now() + timezone.timedelta(days=-1)
        tomorrow = timezone.now() + timezone.timedelta(days=1)
        for mail in res["results"]:
            send_datetime = yesterday
            send_text = mail['location']['timezone']['description']
            filter_customer = mail['gender']
            stop_send_datetime = tomorrow
            mailing.append({
                "send_datetime": send_datetime,
                "send_text": send_text,
                "filter_customer": filter_customer,
                "stop_send_datetime": stop_send_datetime,
            })
        return mailing
