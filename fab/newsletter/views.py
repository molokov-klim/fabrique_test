from django.core.handlers import exception
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict

from rest_framework import status, viewsets, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from .serializers import *
from .models import *
from .core.operational import *

# Создание экземпляра класса
aux = Auxiliary()


# CRUD операции с таблицей "Клиенты"
@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_id="Get list of customers",
    operation_description='GET http://{host}/api/v2/customer/',
    operation_summary='Получение списка всех строк из таблицы "Клиенты"',
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    operation_id="Add new customer",
    operation_description='POST http://{host}/api/v2/customer/',
    operation_summary='Добавление новой строки в таблице "Клиенты"',
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    operation_id="Get a specific customer",
    operation_description='GET http://{host}/api/v2/customer/{id:int}/',
    operation_summary='Получить строку из таблицы "Клиенты" по ID',
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    operation_id="Update customer",
    operation_description='PUT http://{host}/api/v2/customer/{id:int}/',
    operation_summary='Обновить строку в таблице "Клиенты"',
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    operation_id="Update the attributes of customer",
    operation_description='PATCH http://{host}/api/v2/customer/{id:int}/',
    operation_summary='Обновить атрибуты строки в таблице "Клиенты"',
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    operation_id="Delete customer",
    operation_description='DELETE http://{host}/api/v2/customer/{id:int}/',
    operation_summary='Удалить строку из таблицы "Клиенты"',
))
class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [permissions.AllowAny]
    my_tags = ["Операции с таблицей 'Клиенты'"]


# CRUD операции с таблицей "Рассылки"
@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_id="Get list of mailing",
    operation_description='GET http://{host}/api/v2/mailing/',
    operation_summary='Получение списка всех строк из таблицы "Рассылки"',
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    operation_id="Add new mailing",
    operation_description='POST http://{host}/api/v2/mailing/',
    operation_summary='Добавление новой строки в таблице "Рассылки"',
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    operation_id="Get a specific mailing",
    operation_description='GET http://{host}/api/v2/mailing/{id:int}/',
    operation_summary='Получить строку из таблицы "Рассылки" по ID',
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    operation_id="Update mailing",
    operation_description='PUT http://{host}/api/v2/mailing/{id:int}/',
    operation_summary='Обновить строку в таблице "Рассылки"',
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    operation_id="Update the attributes of mailing",
    operation_description='PATCH http://{host}/api/v2/mailing/{id:int}/',
    operation_summary='Обновить атрибуты строки в таблице "Рассылки"',
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    operation_id="Delete mailing",
    operation_description='DELETE http://{host}/api/v2/mailing/{id:int}/',
    operation_summary='Удалить строку из таблицы "Рассылки"',
))
class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer
    permission_classes = [permissions.AllowAny]
    my_tags = ["Операции с таблицей 'Рассылки'"]


# Схема ответа метода StatMailingAll(APIView)
StatMailingAll_response_schema_dict = {
    "200": openapi.Response(
        description="успешное выполнение",
        schema=MailingStatAllSerializer,
        examples={
            "application/json": {
                "send_datetime": "2022-08-05T07:14:06.022Z",
                "send_text": "string",
                "filter_customer": "string",
                "stop_send_datetime": "2022-08-05T07:14:06.022Z",
                "sended": "integer",
                "unsended": "integer",
            }
        },
        extra="extra"
    ),
}


# Получение общей статистики по созданным рассылкам и количеству отправленных сообщений по ним с группировкой по статусам
class StatMailingAll(APIView):
    my_tags = ["Получение статистики по рассылкам"]

    @swagger_auto_schema(
        operation_id="Get general mailing statistic",
        operation_description='GET http://{host}/api/v2/stat_mailing_all/',
        operation_summary='Получение общей статистики по созданным рассылкам и количеству отправленных сообщений по ним с группировкой по статусам',
        responses=StatMailingAll_response_schema_dict,
    )
    def get(self, request):
        result = aux.stat_mailing_all()
        if not result:
            raise exception.Http404
        else:
            return Response(result)


# Схема ответа метода StatMailingSelected(APIView)
StatMailingSelected_response_schema_dict = {
    "200": openapi.Response(
        description="успешное выполнение",
        schema=MailingStatSelected,
        examples={
            "application/json": [{
                'id': 0,
                'time_create': "2022-08-05T07:14:06.022Z",
                'status': True,
                'mailing': 0,
                'customer': 0}]
        },
        extra="extra"
    ),
}


# получение детальной статистики отправленных сообщений по конкретной рассылке
class StatMailingSelected(APIView):
    my_tags = ["Получение статистики по рассылкам"]

    @swagger_auto_schema(
        operation_id="Get selected mailing statistic",
        operation_description='GET http://{host}/api/v2/stat_mailing_selected/{id}/',
        operation_summary='Получение детальной статистики отправленных сообщений по конкретной рассылке',
        responses=StatMailingSelected_response_schema_dict,
    )
    def get(self, request, **kwargs):
        result = aux.stat_mailing_selected(kwargs['pk'])
        if not result:
            raise exception.Http404
        else:
            return Response(result)

