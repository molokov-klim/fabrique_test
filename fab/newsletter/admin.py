from django.contrib import admin
from .models import *


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone', 'operator_code', 'tag', 'UTC')
    list_display_links = ('id', )
    search_fields = ('phone', 'tag')
    list_editable = ('phone', 'operator_code', 'tag', 'UTC')
    list_filter = ('operator_code', 'tag')

class MailingAdmin(admin.ModelAdmin):
    list_display = ('id', 'send_datetime', 'send_text', 'filter_customer', 'stop_send_datetime')
    list_display_links = ('id', )
    search_fields = ('filter_customer',)
    list_editable = ('send_datetime', 'send_text', 'filter_customer', 'stop_send_datetime')
    list_filter = ('send_datetime', 'filter_customer', 'stop_send_datetime')

class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'time_create', 'status', 'mailing', 'customer')
    list_display_links = ('id', )
    list_editable = ('time_create', 'status')
    list_filter = ('time_create', 'status')


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Mailing, MailingAdmin)
admin.site.register(Message, MessageAdmin)
