from django.db import models
from django.core.exceptions import ValidationError
from django.urls import reverse
from rest_framework import serializers
import re

class Customer(models.Model):
    phone = models.CharField(max_length=11, verbose_name="Номер телефона", unique=True)
    operator_code = models.CharField(max_length=3, verbose_name="Код мобильного оператора")
    tag = models.CharField(max_length=255, verbose_name="Тэг")
    UTC = models.CharField(max_length=6, verbose_name="Часовой пояс")

    def __str__(self):
        return str({"id": self.pk, "phone": self.phone, "operator_code": self.operator_code, "tag": self.tag, "UTC": self.UTC})

    def get_absolute_url(self):
        return f"/customer/{self.pk}"
        #f"/customer/{self.pk}/" - OK
        #return reverse("customer-detail", kwargs={'pk': self.pk})
        #return reverse('retrieve', args=[str(self.id)])

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
        ordering = ['tag', ]

    def save(self, **kwargs):
        self.clean()
        return super(Customer, self).save(**kwargs)

    def clean(self):
        super(Customer, self).clean()
        errors = {}
        valphone = str(self.phone)
        # valmail = self.email
        # regex = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9_-]+\.\w{2,3}$'
        if not len(valphone) == 11 or valphone[0] != "7" or not valphone.isdigit():
            errors['phone'] = (f"{valphone} должен содержать 11 символов, начинаться с 7, а также содержать только цифры (7ХХХХХХХХХХ)")
        # if not (re.search(regex, valmail)):
        #     errors['email'] = ('Invalid email id format')
        if errors:
            raise serializers.ValidationError(errors)



class Mailing(models.Model):
    send_datetime = models.DateTimeField(auto_now=False, auto_now_add=False,
                                         verbose_name="Дата и время запуска рассылки")
    send_text = models.TextField(blank=True, verbose_name="Текст сообщения для доставки клиенту")
    filter_customer = models.CharField(max_length=100, verbose_name="Фильтр свойств клиентов")
    stop_send_datetime = models.DateTimeField(auto_now=False, auto_now_add=False,
                                              verbose_name="Дата и время окончания рассылки")

    def __str__(self):
        return str({"id": self.pk, "send_datetime": self.send_datetime, "send_text": self.send_text,
                    "filter_customer": self.filter_customer, "stop_send_datetime": self.stop_send_datetime})

    def get_absolute_url(self):
        return f"/mailing/{self.pk}"

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
        ordering = ['-send_datetime', 'filter_customer']


class Message(models.Model):
    time_create = models.DateTimeField(verbose_name="Дата и время создания (отправки)")
    status = models.BooleanField(default=False, verbose_name="Cтатус отправки")
    mailing = models.ForeignKey('Mailing', on_delete=models.CASCADE, verbose_name="ID рассылки")
    customer = models.ForeignKey('Customer', on_delete=models.CASCADE, verbose_name="ID клиента")

    def __str__(self):
        return str({"id": self.pk, "time_create": self.time_create, "status": self.status, "mailing_id": self.mailing,
                    "customer_id": self.customer})

    def get_absolute_url(self):
        return f"/message/{self.pk}"

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['time_create', ]


class BackgroundTask(models.Model):
    id = models.BigAutoField(primary_key=True)
    task_name = models.CharField(max_length=190)
    task_params = models.TextField()
    task_hash = models.CharField(max_length=40)
    verbose_name = models.CharField(max_length=255, blank=True, null=True)
    priority = models.IntegerField()
    run_at = models.DateTimeField()
    repeat = models.BigIntegerField()
    repeat_until = models.DateTimeField(blank=True, null=True)
    queue = models.CharField(max_length=190, blank=True, null=True)
    attempts = models.IntegerField()
    failed_at = models.DateTimeField(blank=True, null=True)
    last_error = models.TextField()
    locked_by = models.CharField(max_length=64, blank=True, null=True)
    locked_at = models.DateTimeField(blank=True, null=True)
    creator_object_id = models.IntegerField(blank=True, null=True)
    creator_content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'background_task'


class MailingFlag(models.Model):
    status = models.BooleanField(default=False, verbose_name="Cтатус отправки")
    mailing = models.ForeignKey('Mailing', on_delete=models.CASCADE, verbose_name="ID рассылки")

    def __str__(self):
        return str({"id": self.pk, "status": self.status, "mailing_id": self.mailing})

    class Meta:
        verbose_name = 'Статус рассылки'
        verbose_name_plural = 'Статус рассылки'
        ordering = ['mailing', ]

class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)