from django.urls import path, re_path, include
from .views import *
from rest_framework import routers
from drf_yasg.views import get_schema_view

router = routers.DefaultRouter()
router.register(r'customer', CustomerViewSet, basename='customer')
router.register(r'mailing', MailingViewSet, basename='mailing')

urlpatterns = [
    path('api/v2/', include(router.urls)),  # http://127.0.0.1:8000/api/v2/customer/
    path('api/v2/stat_mailing_all/', StatMailingAll.as_view(), name="stat_mailing_all"),
    path('api/v2/stat_mailing_selected/<int:pk>/', StatMailingSelected.as_view(), name="stat_mailing_selected"),
]
