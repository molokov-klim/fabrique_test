from rest_framework import serializers
from .models import Customer, Mailing, Message


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = "__all__"


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"


class MailingStatAllSerializer(serializers.Serializer):
    send_datetime = serializers.DateTimeField()
    send_text = serializers.CharField()
    filter_customer = serializers.CharField(max_length=100)
    stop_send_datetime = serializers.DateTimeField()
    sended = serializers.IntegerField()
    unsended = serializers.IntegerField()

class MailingStatSelected(serializers.Serializer):
    id = serializers.IntegerField()
    time_create = serializers.DateTimeField()
    status = serializers.BooleanField()
    mailing = serializers.IntegerField()
    customer = serializers.IntegerField()


