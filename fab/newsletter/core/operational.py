import requests
import datetime

from drf_yasg.inspectors import SwaggerAutoSchema
from rest_framework.response import Response

from newsletter.models import *
from django.forms.models import model_to_dict
from newsletter.config import API_TOKEN
from django.utils import timezone
import pytz


class CustomAutoSchema(SwaggerAutoSchema):

    def get_tags(self, operation_keys=None):
        tags = self.overrides.get('tags', None) or getattr(self.view, 'my_tags', [])
        if not tags:
            tags = [operation_keys[0]]
        return tags


class Auxiliary:
    def __init__(self):
        pass

    def validate_phone(self, phone):
        if (len(phone) == 11 and phone.isdigit() == True and phone[0] == "7"):
            return True
        else:
            return False

    def extract_operator_code(self, phone):
        operator_code = phone[1] + phone[2] + phone[3]
        return operator_code

    def convert_to_datetime(self, dt_string):
        format_dt = "%Y-%m-%d %H:%M:%S"
        dt_object = datetime.datetime.strptime(dt_string, format_dt)
        dt_object = dt_object.replace(tzinfo=None)
        return dt_object

    def stat_mailing_all(self):
        try:
            result = []
            for i in Mailing.objects.all():
                row_mailing = model_to_dict(i)
                mailing_id = int(row_mailing['id'])
                list_message_true = []
                list_message_false = []
                for y in Message.objects.filter(mailing_id=mailing_id):
                    row_message = model_to_dict(y)
                    if row_message['status']:
                        list_message_true.append(row_message)
                    else:
                        list_message_false.append(row_message)
                result.append({
                    "id": row_mailing['id'],
                    'send_datetime': row_mailing['send_datetime'],
                    'send_text': row_mailing['send_text'],
                    'filter_customer': row_mailing['filter_customer'],
                    'stop_send_datetime': row_mailing['stop_send_datetime'],
                    'sended: ': len(list_message_true), 'unsended': len(list_message_false)})
            return result
        except Exception as _ex:
            return Response(f"[SYSTEM] Error in views: {_ex}")

    def stat_mailing_selected(self, pk):
        result = []
        try:
            for i in Message.objects.filter(mailing=pk):
                row_message = model_to_dict(i)
                result.append(row_message)
            return result
        except Exception as _ex:
            return Response(f"[SYSTEM] Error in views: {_ex}")


class Sending:
    def __init__(self):
        pass

    # актуализация статуса рассылок
    def update_mailing_status(self):
        for i in MailingFlag.objects.filter(status=False):
            row_mailing = model_to_dict(i)
            if Message.objects.filter(mailing=row_mailing['mailing']):
                if Message.objects.filter(mailing=row_mailing['mailing']).count() == Message.objects.filter(
                        mailing=row_mailing['mailing'], status=True).count():
                    push = MailingFlag.objects.get(mailing=row_mailing['mailing'])
                    push.status = True
                    push.save()
        return

    # создание записей в таблице статуса рассылок
    def create_mailing_flag_rows(self):
        for i in Mailing.objects.all():
            row_mailing = model_to_dict(i)
            if not MailingFlag.objects.filter(mailing=row_mailing['id']):
                push = MailingFlag.objects.create(mailing_id=row_mailing['id'], status=False)
                push.save()
        return

    # выбирает из таблицы "Рассылка" диапазон строк где текущее время больше времени начала и меньше времени окончания
    def extract_range_from_mailing(self):
        Sending.create_mailing_flag_rows(self)
        mailing_range = Mailing.objects.filter(send_datetime__lte=timezone.now(),
                                               stop_send_datetime__gte=timezone.now())
        result = []
        for i in mailing_range:
            row_mailing = model_to_dict(i)
            if MailingFlag.objects.filter(mailing=row_mailing['id'], status=False):
                result.append(row_mailing)
        return result

    # выбирает из таблицы "Клиенты" диапазон строк, подходящих под значение фильтра
    @staticmethod
    def extract_range_from_customer(filter_customer):
        filter_customer_type = Sending.define_filter(filter_customer)
        result = []
        if filter_customer_type == "tag":
            for i in Customer.objects.filter(tag=filter_customer):
                row_customer = model_to_dict(i)
                result.append(row_customer)
        if filter_customer_type == "operator_code":
            for i in Customer.objects.filter(operator_code=filter_customer):
                row_customer = model_to_dict(i)
                result.append(row_customer)
        return result

    # проверяет тип фильтра
    @staticmethod
    def define_filter(filter_customer):
        if filter_customer.isdigit() and len(filter_customer) == 3:
            return "operator_code"
        else:
            return "tag"

    # создает сообщение для рассылки
    @staticmethod
    def create_message(row_mailing, row_customer):
        # если в списке сообщений нет идентичного то добавление нового сообщения с флагом False
        if not Message.objects.filter(mailing_id=row_mailing['id'], customer_id=row_customer['id']):
            push = Message.objects.create(time_create=timezone.now(), status=False,
                                          customer_id=row_customer['id'], mailing_id=row_mailing['id'])
            push.save()
        return

    # проверяет список рассылок и добавляет новые записи в список сообщений, если требуется
    def handle_mailing(self):
        mailing_range = Sending.extract_range_from_mailing(self)
        for row_mailing in mailing_range:
            customer_range = Sending.extract_range_from_customer(row_mailing['filter_customer'])
            for row_customer in customer_range:
                Sending.create_message(row_mailing, row_customer)
        Sending.update_mailing_status(self)
        return

    # отправляет запрос на внешний API, возвращает True/False
    @staticmethod
    def send_to_cloud(cloud_id, cloud_text, cloud_phone):
        url = f"https://probe.fbrq.cloud/v1/send/{cloud_id}"
        headers = {'Authorization': 'Bearer {}'.format(API_TOKEN)}
        resp = requests.post(url, json={"id": cloud_id, "phone": int(cloud_phone), "text": cloud_text},
                             headers=headers)
        if resp.status_code == 200:
            return True
        else:
            return False
