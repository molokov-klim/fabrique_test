from background_task import background
from newsletter.core.operational import Sending
import datetime
import time
from django.forms.models import model_to_dict
from newsletter.models import *


# бесконечная петля
# выбирает из таблицы "newsletter_message" все сообщения и для каждой строки вызывает send_to_cloud() которая
# отправляет запрос на внешний API, статус код ответа сервера записывается в поле status строки
@background()
def loop_sending():
    print("[SYS] Start loop_sending")
    while True:
        print(f'[SYS]{datetime.datetime.now()} loop_sending()')
        for i in Message.objects.filter(status=False):
            row_message = model_to_dict(i)
            print("row_message: ", row_message)
            cloud_id = row_message['id']
            send_text = Mailing.objects.filter(id=row_message['mailing'])
            send_text = model_to_dict(send_text[0])
            cloud_text = send_text['send_text']
            phone = Customer.objects.filter(id=row_message['customer'])
            phone = model_to_dict(phone[0])
            cloud_phone = phone['phone']
            print("cloud_id, cloud_text, cloud_phone: ", cloud_id, cloud_text, cloud_phone)
            result = Sending.send_to_cloud(cloud_id, cloud_text, cloud_phone)
            print("result: ", result)
            message = Message.objects.get(id=cloud_id)
            message.status = result
            message.save()
        time.sleep(10)


# бесконечная петля
# проверяет список рассылок и добавляет новые записи в список сообщений, если требуется
@background()
def loop_handle_mailing():
    print("[SYS] loop_handle_mailing")
    send = Sending()
    while True:
        print(f'[SYS]{datetime.datetime.now()} loop_handle_mailing()')
        send.handle_mailing()
        time.sleep(60)

# очищает список задач
# единожды, при запуске сервера
def truncate_background_task():
    print("[SYS] TRUNCATE background_task")
    BackgroundTask.objects.all().delete()

